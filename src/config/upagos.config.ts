import { registerAs } from '@nestjs/config';

export default registerAs('upagos', () => ({
  debtsUrl: process.env.DEBTS_URL,
  paymentUrl: process.env.PAYMENT_URL,
  token: process.env.TOKEN,
}));

import { IsArray, IsNotEmpty, ValidateNested } from 'class-validator';
import { Consumption } from './consumption.dto';
import { Customer } from './customer.dto';

export class Client extends Customer {
  @IsArray()
  @IsNotEmpty()
  @ValidateNested()
  consumptions: Consumption[];
}

import { IsNotEmpty, IsString } from 'class-validator';

export class Customer {
  @IsString({
    message:
      'El remoteId debe ser un string, preferiblemente el rut del cliente',
  })
  @IsNotEmpty({ message: 'El remoteId es un campo obligatorio' })
  remoteId: string;

  @IsString({ message: 'El nombre es un string' })
  @IsNotEmpty({ message: 'El nombre es un campo obligatorio' })
  name: string;

  @IsString({ message: 'El nationalId es un string' })
  @IsNotEmpty({ message: 'El nationalId es un campo obligatorio' })
  nationalId: string;

  @IsString({ message: 'El email es un string' })
  email: string;

  @IsString({ message: 'El mobile es un string' })
  mobile: string;
}

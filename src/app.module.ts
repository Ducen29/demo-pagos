import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UpagosModule } from './modules/upagos/upagos.module';
import { PaytechModule } from './modules/paytech/paytech.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { getEnv } from './config/env.config';
import dbConfig from './config/db.config';
import upagosConfig from './config/upagos.config';
import serverConfig from './config/server.config';
import { MongooseModule } from '@nestjs/mongoose';
import {
  Transaction,
  TransactionSchema,
} from './modules/upagos/models/transaction';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: getEnv(),
      load: [dbConfig, upagosConfig, serverConfig],
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          uri: configService.get<string>('db.mongoUrl'),
        };
      },
    }),
    {
      ...MongooseModule.forFeature([
        { name: Transaction.name, schema: TransactionSchema },
      ]),
      global: true,
    },
    UpagosModule,
    HttpModule,
    PaytechModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

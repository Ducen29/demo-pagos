import { Controller, Get, Param, Query, Render } from '@nestjs/common';
import { AppService } from './app.service';
import { UpagosService } from 'src/modules/upagos/upagos.service';
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private upaySvc: UpagosService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('form/:id')
  @Render('form')
  async form(@Param('id') id: string) {
    const data = await this.appService.form(id);
    return { message: 'Hola', ...data };
  }

  @Get('/result')
  @Render('success')
  async rederTransaction(
    @Query('token') token,
    @Query('status') status: string,
  ) {
    const data = await this.upaySvc.findTransactionInfo(token, status);
    let message = '';

    if(status != 'PAID' && status != 'PENDING'){
      message = "Pago Rechazado"
    }else{
      message = "Pago exitoso";
    }
    return {data,message};
  }
}

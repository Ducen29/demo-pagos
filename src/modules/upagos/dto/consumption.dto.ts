import { IsArray, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Item } from './item.dto';

export class Consumption {
  @IsString({
    message:
      'El remoteId debe ser un string, preferiblemente el código del a deuda',
  })
  @IsNotEmpty({ message: 'El remoteId es un campo obligatorio' })
  remoteId: string;

  @IsString({ message: 'La descripción es un string' })
  @IsNotEmpty({ message: 'La descripción es un campo obligatorio' })
  description: string;

  @IsString({ message: 'La currency es un string' })
  @IsNotEmpty({ message: 'La currency es un campo obligatorio' })
  currency: string;

  @IsArray()
  @IsNotEmpty()
  @ValidateNested()
  items: Item[];
}

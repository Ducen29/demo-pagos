import { IsDate, IsString } from 'class-validator';

export class GatewayResponse {
  @IsString({ message: 'El transactionId es un string' })
  transactionId: string;
  @IsString({ message: 'El paymentMethodCode es un string' })
  paymentMethodCode: string;
  @IsDate()
  accountingDate: Date;
}

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { UpagosController } from './upagos.controller';
import { UpagosService } from './upagos.service';

@Module({
  imports: [HttpModule],
  controllers: [UpagosController],
  providers: [UpagosService],
  exports:[UpagosService]
})
export class UpagosModule {}

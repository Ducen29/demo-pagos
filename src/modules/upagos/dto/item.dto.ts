import { IsDate, IsNotEmpty, IsNumber, IsString } from 'class-validator';
export class Item {
  @IsString({
    message:
      'El remoteId debe ser un string, preferiblemente el código del a deuda',
  })
  @IsNotEmpty({ message: 'El remoteId es un campo obligatorio' })
  remoteId: string;

  @IsString({ message: 'La descripción es un string' })
  @IsNotEmpty({ message: 'La descripción es un campo obligatorio' })
  description: string;

  @IsNumber({ maxDecimalPlaces: 1 })
  @IsNotEmpty({ message: 'El amount no puede estar vacio' })
  amount: number;

  @IsNumber({ maxDecimalPlaces: 1 })
  @IsNotEmpty({ message: 'El penaltyAmount es un campo obligatorio' })
  penaltyAmount: number;

  @IsNumber({ maxDecimalPlaces: 0 })
  @IsNotEmpty({ message: 'El penaltyDays es un campo obligatorio' })
  penaltyDays: number;

  @IsString({ message: 'El penaltyType debe ser string' })
  penaltyType: string;

  @IsNumber({ maxDecimalPlaces: 1 })
  @IsNotEmpty({
    message: 'El prejudicialCollectionAmount es un campo obligatorio',
  })
  prejudicialCollectionAmount: number;

  @IsNumber({ maxDecimalPlaces: 1 })
  @IsNotEmpty({ message: 'El balance es un campo obligatorio' })
  balance: number;

  @IsDate()
  @IsNotEmpty({ message: 'El issueDate es un campo obligatorio' })
  issueDate: Date;

  @IsDate()
  valuationAt: Date;

  @IsDate()
  @IsNotEmpty({ message: 'El expirationDate es un campo obligatorio' })
  expirationAt: Date;

  @IsNumber({ maxDecimalPlaces: 0 })
  minimumPay: number;

  @IsNumber({ maxDecimalPlaces: 0 })
  maximumPay: number;
}

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  Transaction,
  TransactionDocument,
} from './modules/upagos/models/transaction';

@Injectable()
export class AppService {
  constructor(
    @InjectModel(Transaction.name)
    private transactionModel: Model<TransactionDocument>,
  ) {}
  getHello(): string {
    return 'Hello World!';
  }

  async form(id: string) {
    const transaction = await this.transactionModel.findById(id);
    return {
      id: transaction._id.toString(),
      token: transaction.token,
      paymentUrl: transaction.paymentLink,
    };
  }
}

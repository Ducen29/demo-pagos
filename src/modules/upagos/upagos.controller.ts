import { Body, Controller, Param, Post, Request } from '@nestjs/common';
import { Client } from './dto/client.dto';
import { ItemPaid } from './dto/item-paid.dto';
import { Transaction } from './dto/transaction.dto';
import { UpagosService } from './upagos.service';

@Controller('upagos')
export class UpagosController {
  constructor(private readonly upagosService: UpagosService) {}
  @Post('inform-debts')
  async informDebts(@Body() body: Client[]) {
    return await this.upagosService.informDebts(body);
  }

  @Post('customer-with-debts')
  async createCustomerWithDebts(@Body() body: Client[]) {
    return await this.upagosService.createClientAndInformDebt(body);
  }

  @Post('notify-external-payment/:rut')
  async notifyExternalPayment(
    @Body() body: { itemsPaid: Partial<ItemPaid> },
    @Param('rut') rut: string,
  ) {
    return await this.upagosService.notifyExternalPayment(body, rut);
  }
  @Post('notify-payment/callback')
  async handleNotificationOfPayment(@Body() body: Transaction) {
    return await this.upagosService.handleNotificationOfPayment(body);
  }
  @Post('notify-resume/callback')
  async handleNotificationResume(@Body() body: Client[]) {
    return await this.upagosService.handleNotificationOfPayment(body);
  }

  @Post('payment-link')
  async paymentLinkRequest(@Body() body: any) {
    return await this.upagosService.paymentLinkRequest(body);
  }

  @Post('payment-link/callback')
  async paymentLinkPaid(@Request() request: any) {
    const body = request.body;
    const auth = request.headers['authorization'];
    console.log(request.headers);
    console.log(auth);
    return await this.upagosService.getResultTransaction(body, auth);
  }
}

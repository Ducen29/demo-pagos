import { IsNotEmpty, IsNumber, ValidateNested } from 'class-validator';
import { Item } from './item.dto';

export class ItemPaid {
  @IsNumber({ maxDecimalPlaces: 1 })
  @IsNotEmpty({ message: 'El paymentAmount es un campo obligatorio' })
  paymentAmount: number;

  @IsNotEmpty()
  @ValidateNested()
  items: Item;
}

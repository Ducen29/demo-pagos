import {
  IsArray,
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Customer } from './customer.dto';
import { GatewayResponse } from './gateway-response.dto';
import { ItemPaid } from './item-paid.dto';

export class Transaction {
  @IsString({ message: 'El transactionId es un string' })
  @IsNotEmpty({ message: 'El transactionId es requerido' })
  transactionId: string;

  @IsString({ message: 'El currency es un string' })
  @IsNotEmpty({ message: 'El currency es requerido' })
  currency: string;

  @IsDate()
  @IsNotEmpty()
  paymentAt: Date;

  @IsDate()
  @IsNotEmpty()
  paymentAccountingDate: Date;

  @IsNumber({ maxDecimalPlaces: 1 })
  @IsNotEmpty({ message: 'El amount es un campo obligatorio' })
  amount: number;

  @IsString({ message: 'El paymentGateway es un string' })
  @IsNotEmpty({ message: 'El paymentGateway es requerido' })
  paymentGateway: string;

  @IsString({ message: 'El status es un string' })
  @IsNotEmpty({ message: 'El status es requerido' })
  status: string;

  @IsNotEmpty()
  @ValidateNested()
  customer: Customer;

  @IsNotEmpty()
  @ValidateNested()
  gatewayResponse: GatewayResponse;

  @IsNotEmpty()
  @IsArray()
  @ValidateNested()
  itemsPaid: ItemPaid[];
}

import { Logger } from '@nestjs/common';
import { join } from 'path';

// eslint-disable-next-line @typescript-eslint/no-var-requires
// require('dotenv').config();

const logger = new Logger();

export function getEnv() {
  const env = '.env';

  const path = join(process.cwd(), 'environment', env);

  logger.log(`Selected env: ${env}`);
  logger.log(`Env file path: ${path}`);

  return path;
}

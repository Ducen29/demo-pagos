import { registerAs } from '@nestjs/config';

export default registerAs('server', () => ({
  baseUrl:
    process.env.NODE_ENV == 'production'
      ? process.env.BASE_URL_HEROKU
      : process.env.BASE_URL_LOCAL,
}));

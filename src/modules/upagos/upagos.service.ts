import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { v4 } from 'uuid';
import { Transaction, TransactionDocument } from './models/transaction';

@Injectable()
export class UpagosService {
  private url = '';
  private paymentUrl = '';
  private token = '';
  constructor(
    private httpService: HttpService,
    @InjectModel(Transaction.name)
    private transactionModel: Model<TransactionDocument>,
    private readonly configService: ConfigService,
  ) {
    this.url = this.configService.get('upagos.debtsUrl');
    this.paymentUrl = this.configService.get('upagos.paymentUrl');
    this.token = this.configService.get('upagos.token');
  }

  async informDebts(data: any) {
    return new Promise<any>((resolve, reject) => {
      this.httpService
        .post(this.url + 'business/customers/bulk/inform-debts', data, {
          headers: { Authorization: 'Bearer ' + this.token },
        })
        .subscribe(
          (data) => {
            console.log('SUCCESS => ', data.data);
            resolve(data.data);
          },
          (error) => {
            console.log('ERROR => ', error.response.data);
            reject('Error al hacer informe');
          },
        );
    });
  }

  async createClientAndInformDebt(data: any) {
    return new Promise<any>((resolve, reject) => {
      this.httpService
        .post(
          this.url + 'business/customers/inform-customer-with-debts',
          data,
          {
            headers: { Authorization: 'Bearer ' + this.token },
          },
        )
        .subscribe(
          (data) => {
            console.log('SUCCESS => ', data.data);
            resolve(data.data);
          },
          (error) => {
            console.log('ERROR => ', error.response.data);
            reject('Error al hacer informe');
          },
        );
    });
  }

  async notifyExternalPayment(data: any, rut: string) {
    return new Promise<any>((resolve, reject) => {
      this.httpService
        .post(
          this.url + `business/customers/${rut}/debts/notify_external_payment`,
          data,
          {
            headers: { Authorization: 'Bearer ' + this.token },
          },
        )
        .subscribe(
          (data) => {
            console.log('SUCCESS => ', data.data);
            resolve(data.data);
          },
          (error) => {
            console.log('ERROR => ', error.response.data);
            reject('Error al hacer informe');
          },
        );
    });
  }

  async paymentLinkRequest(data: any) {
    return new Promise<any>(async (resolve, reject) => {
      const insertData: Transaction = {
        amount: data.amount,
        currency: data.currency,
        customerEmail: data.customer.email,
        customerName: data.customer.name,
        customerPhone: data.customer.mobile,
        customerRut: data.customer.nationalId,
      };

      const transaction = await this.transactionModel.create(insertData);
      data.transactionIdOnClient = transaction._id.toString();
      data.consumptions.forEach((e: any, index: string | number) => {
        data.consumptions[index].remoteId = v4();
        data.consumptions[index].items.forEach(
          (i: any, index2: string | number) => {
            data.consumptions[index].items[index2].remoteId = v4();
          },
        );
      });
      console.log(data);
      this.httpService
        .post(this.paymentUrl, data, {
          headers: { Authorization: 'Bearer ' + this.token },
        })
        .subscribe(
          async (data) => {
            console.log(data.data);
            transaction.paymentLink = data.data.url;
            transaction.token = data.data.token;
            transaction.status = 'pending';
            await transaction.save();
            resolve({
              url: `${this.configService.get(
                'server.baseUrl',
              )}/form/${transaction._id.toString()}`,
            });
          },
          (error) => {
            console.log(error);
            reject('Error al intentar la petición de link de pago');
          },
        );
    });
  }

  async getResultTransaction(body: any, header: any) {
    try {
      console.log(this.token);
      if (header != this.token) throw new Error('Unauthorized');
      const transaction = await this.transactionModel.findOne({
        token: body.token,
      });

      if (!transaction) throw new Error('Transaccion no encontrada');
      if (body.status == 'PAID') transaction.status = 'paid';
      else transaction.status = 'rejected';

      transaction.paymentMethod = body.paymentGateway;
      await transaction.save();
      return { message: 'Ok' };
    } catch (error) {
      console.log(error);
      throw new Error('Error interno');
    }
  }

  async handleNotificationOfPayment(data: any) {
    console.log(data);
    return { message: 'Ok', code: 200 };
  }

  async handleNotificationOfResume(data: any) {
    console.log(data);
    return { message: 'Ok', code: 200 };
  }

  async findTransactionInfo(token: string, status: string) {
    const transaction = await this.transactionModel.findOne({
      token: token,
    });
    if (!transaction) throw new Error('Transaccion no encontrada');
    if (status == 'PAID') transaction.status = 'paid';
    else transaction.status = 'rejected';
    return transaction;
  }
}

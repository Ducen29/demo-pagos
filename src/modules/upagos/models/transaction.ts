import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TransactionDocument = Transaction & Document;

@Schema()
export class Transaction {
  @Prop()
  amount: number;

  @Prop()
  currency: string;

  @Prop()
  customerRut: string;

  @Prop()
  customerName: string;

  @Prop()
  customerEmail: string;

  @Prop()
  customerPhone: string;

  @Prop({ default: '', required: false })
  paymentLink?: string;

  @Prop({ default: '', required: false })
  token?: string;

  @Prop({ default: 'created', required: false })
  status?: string;

  @Prop({ default: '', required: false })
  paymentMethod?: string;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
